import { Module } from '@nestjs/common';
import { ConstantsService } from '@app/constants/constants.service';

@Module({
  providers: [ConstantsService],
  exports: [ConstantsService],
})
export class ConstantsModule {}
