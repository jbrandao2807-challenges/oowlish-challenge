import { Injectable } from '@nestjs/common';

@Injectable()
export class ConstantsService {
  public readonly GOOGLE_MAPS_URL = 'GOOGLE_MAPS_URL';
  public readonly GOOGLE_MAPS_SERVICE_URL = 'GOOGLE_MAPS_SERVICE_URL';
  public readonly REQUEST_DENIED = 'REQUEST_DENIED';
  public readonly GOOGLE_MAPS_API_KEY = 'GOOGLE_MAPS_API_KEY';
  public readonly GOOGLE_MAPS_SERVICE_PORT = 'GOOGLE_MAPS_SERVICE_PORT';
  public readonly CUSTOMERS_SERVICE_PORT = 'CUSTOMERS_SERVICE_PORT';
}
