import { Injectable } from '@nestjs/common';
import {
  ApiResponseErrorFormat,
  ApiResponseSuccessFormat,
} from '@app/api-response-formatter/ApiResponseFormat';

@Injectable()
export class ApiResponseFormatterService {
  formatSuccessResponse(results: any[]): ApiResponseSuccessFormat {
    return { results, count: results.length };
  }

  formatErrorResponse(error: Error): ApiResponseErrorFormat {
    return { message: error.message, error: error.name };
  }
}
