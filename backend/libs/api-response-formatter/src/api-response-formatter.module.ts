import { Module } from '@nestjs/common';
import { ApiResponseFormatterService } from './api-response-formatter.service';

@Module({
  providers: [ApiResponseFormatterService],
  exports: [ApiResponseFormatterService],
})
export class ApiResponseFormatterModule {}
