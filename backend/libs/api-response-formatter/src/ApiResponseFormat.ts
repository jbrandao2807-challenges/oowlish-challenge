export type ApiResponseFormat = ApiResponseSuccessFormat |
  ApiResponseErrorFormat;

export interface ApiResponseErrorFormat {
  message: string;
  error: string;
}

export interface ApiResponseSuccessFormat {
  count: number;
  results: any[];
}
