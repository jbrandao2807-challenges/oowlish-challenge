import { Controller, Get, Logger, Query } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { ApiResponseFormatterService } from '@app/api-response-formatter';
import { ApiResponseFormat } from '@app/api-response-formatter/ApiResponseFormat';

@Controller('/customers')
export class CustomersController {
  private logger: Logger;

  constructor(
    private readonly customersService: CustomersService,
    private readonly apiResponseFormatter: ApiResponseFormatterService,
  ) {
    this.logger = new Logger(CustomersService.name);
  }

  @Get()
  async findById(@Query('id') id: string): Promise<ApiResponseFormat> {
    try {
      const customer = await this.customersService.findById(parseInt(id));
      return this.apiResponseFormatter.formatSuccessResponse([customer]);
    } catch (e) {
      return this.apiResponseFormatter.formatErrorResponse(e);
    }
  }

  @Get('/cities/totals')
  getTotalCustomersByCity() {
    try {
      const totalCustomersByCity = this.customersService.getCustomersTotalsByCity();
      return this.apiResponseFormatter.formatSuccessResponse(
        totalCustomersByCity,
      );
    } catch (e) {
      return this.apiResponseFormatter.formatErrorResponse(e);
    }
  }

  @Get('/cities')
  listCustomersByCity(@Query('name') city: string) {
    try {
      const customers = this.customersService.listCustomersByCity(city);
      return this.apiResponseFormatter.formatSuccessResponse(
        customers,
      );
    } catch (e) {
      return this.apiResponseFormatter.formatErrorResponse(e);
    }

  }
}
