import { HttpService, Injectable, Logger } from '@nestjs/common';
import { CustomerDto } from './customer/customer-dto';
import allCustomers from './costumers.mock';
import { ConfigService } from '@nestjs/config';
import { ConstantsService } from '@app/constants';

@Injectable()
export class CustomersService {
  private logger: Logger;

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private constantsService: ConstantsService,
  ) {
    this.logger = new Logger(CustomersService.name);
  }

  async findById(id: number): Promise<CustomerDto | undefined> {
    const foundIndex = allCustomers.findIndex((customer) => {
      return customer.id === id;
    });
    if (foundIndex === -1) {
      return undefined;
    }
    const foundCustomer = allCustomers[foundIndex];
    const url = `${this.configService.get(
      this.constantsService.GOOGLE_MAPS_SERVICE_URL,
    )}/geocoding`;
    let customerLocation = { latitude: '', longitude: '' };
    try {
      const apiResponse = await this.httpService
        .get(url, { params: { address: foundCustomer.city } })
        .toPromise();
      if (apiResponse?.data?.results?.length > 0) {
        customerLocation = apiResponse.data.results[0];
      }
    } catch ({ message, name }) {
      this.logger.error(
        `Unable to get location for customer: ${foundCustomer.id}. ${message}`,
      );
    }

    return new CustomerDto({
      ...foundCustomer,
      latitude: customerLocation.latitude,
      longitude: customerLocation.longitude,
    });
  }

  getCustomersTotalsByCity() {
    const customersCount = allCustomers.reduce(
      (previousValue, currentValue) => {
        const cityCount = previousValue[currentValue.city];
        previousValue[currentValue.city] =
          cityCount && cityCount > 0 ? cityCount + 1 : 1;
        return previousValue;
      },
      {},
    );
    return Object.keys(customersCount).map((key) => ({
      city: key,
      customers_total: customersCount[key],
    }));
  }

  listCustomersByCity(city: string) {
    const customers = JSON.parse(JSON.stringify(allCustomers));
    return customers.filter((item) => item.city === city);
  }
}
