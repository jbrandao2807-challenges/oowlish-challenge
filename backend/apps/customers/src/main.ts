import { NestFactory } from '@nestjs/core';
import { CustomersModule } from './customers.module';
import { ConfigService } from '@nestjs/config';
import { ConstantsService } from '@app/constants';

async function bootstrap() {
  const isDevelopment = process.env.NODE_ENV === 'development';
  const app = await NestFactory.create(CustomersModule, {
    logger: isDevelopment
      ? ['log', 'error', 'warn', 'debug', 'verbose']
      : ['log', 'error'],
  });
  const configService = app.get(ConfigService);
  const constantsService = app.get(ConstantsService);
  const port = configService.get(
    constantsService.CUSTOMERS_SERVICE_PORT.toString(),
  );
  app.enableCors();
  await app.listen(port);
}

bootstrap();
