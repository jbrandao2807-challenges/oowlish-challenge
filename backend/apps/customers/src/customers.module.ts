import { HttpModule, Module } from '@nestjs/common';
import { CustomersController } from './customers.controller';
import { CustomersService } from './customers.service';
import { CustomerResolver } from './customer/customer.resolver';
import { GraphQLModule } from '@nestjs/graphql';
import { ConfigModule } from '@nestjs/config';
import { ApiResponseFormatterModule } from '@app/api-response-formatter';
import { ConstantsModule } from '@app/constants';

@Module({
  imports: [
    GraphQLModule.forRoot({ typePaths: ['./**/*.graphql'] }),
    ConfigModule.forRoot({ isGlobal: true }),
    ApiResponseFormatterModule,
    ConstantsModule,
    HttpModule,
  ],
  controllers: [CustomersController],
  providers: [CustomersService, CustomerResolver],
})
export class CustomersModule {}
