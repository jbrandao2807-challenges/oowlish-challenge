import { CustomersService } from './customers.service';
import { HttpService } from '@nestjs/common';
import { ConstantsService } from '@app/constants';
import { ConfigService } from '@nestjs/config';
import { Subject } from 'rxjs';
import { AxiosResponse } from 'axios';
import { CustomerDto } from './customer/customer-dto';
import allCustomers from './costumers.mock';

describe('CustomersService', () => {
  let customersService: CustomersService;
  let httpService: HttpService;
  let configService: ConfigService;
  const mockedLongLat = {
    results: [{
      latitude: 0,
      longitude: 0,
    }],
  };
  const axiosResponse = {
    data: mockedLongLat,
    status: 200,
    statusText: 'OK',
    headers: {},
    config: {},
  };
  beforeEach(() => {
    configService = new ConfigService<Record<string, any>>();
    httpService = new HttpService();
    customersService = new CustomersService(httpService, configService, new ConstantsService());
  });

  describe('findById', () => {
    it('It should send empty coordinates when error on google-maps', async (cb) => {


      const subject = new Subject<AxiosResponse<unknown>>();
      setTimeout(() => {
        subject.next(axiosResponse);
        subject.error(new Error());

      }, 500);
      jest.spyOn(httpService, 'get').mockImplementation(() => {
        return subject.asObservable();
      });

      const found = await customersService.findById(1);
      expect(found).toEqual(new CustomerDto({
        ...allCustomers[0],
        latitude: '',
        longitude: '',
      }));
      cb();

    });
    it('It should send empty coordinates when error on google-maps', async (cb) => {


      const subject = new Subject<AxiosResponse<unknown>>();
      setTimeout(() => {
        subject.next(axiosResponse);
        subject.complete();

      });
      jest.spyOn(httpService, 'get').mockImplementation(() => {
        return subject.asObservable();
      });

      const found = await customersService.findById(1);
      expect(found).toEqual(new CustomerDto({
        ...allCustomers[0],
        latitude: 0,
        longitude: 0,
      }));
      cb();

    });
  });
});