export class CustomerDto {
  constructor({
    id,
    first_name,
    last_name,
    email,
    gender,
    company,
    city,
    title,
    latitude,
    longitude,
  }) {
    this.id = id;
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.gender = gender;
    this.company = company;
    this.city = city;
    this.title = title;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public readonly id: number;
  public readonly first_name: string;
  public readonly last_name: string;
  public readonly email: string;
  public readonly gender: string;
  public readonly company: string;
  public readonly city: string;
  public readonly title: string;
  public readonly latitude?: string;
  public readonly longitude?: string;
}
