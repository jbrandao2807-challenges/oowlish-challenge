import { Args, Query, Resolver } from '@nestjs/graphql';
import { CustomersService } from '../customers.service';

@Resolver('Customer')
export class CustomerResolver {
  constructor(private customersService: CustomersService) {
  }

  @Query()
  async customer(@Args('id') id: number) {
    return this.customersService.findById(id);
  }
}
