import { CustomersController } from './customers.controller';
import { CustomersService } from './customers.service';
import { ApiResponseFormatterService } from '@app/api-response-formatter';

describe('CustomersController', () => {
  let customerController: CustomersController;
  let customersService: CustomersService;
  let apiFormatter: ApiResponseFormatterService;

  beforeEach(() => {
    apiFormatter = new ApiResponseFormatterService();
    customersService = new CustomersService();
    customerController = new CustomersController(customersService, apiFormatter);
  });

  describe('findById', () => {
    it('should return the formatted response from api formatter on success', async () => {
      const expected = { test: 1 };
      jest.spyOn(apiFormatter, 'formatSuccessResponse').mockImplementation(() => {
        return expected;
      });
      jest.spyOn(customersService, 'findById').mockImplementation(() => {
        return {};
      });
      expect(await customerController.findById('10')).toBe(expected);
    });
    it('should return the error response from api formatter on error', async () => {
      const expected = { test: 1 };
      jest.spyOn(apiFormatter, 'formatErrorResponse').mockImplementation(() => {
        return expected;
      });
      jest.spyOn(customersService, 'findById').mockImplementation(args => {
        throw new Error();
      });
      expect(await customerController.findById('10')).toBe(expected);
    });
  });

});