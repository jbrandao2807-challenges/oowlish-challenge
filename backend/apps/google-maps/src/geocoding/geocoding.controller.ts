import { Controller, Get, Query } from '@nestjs/common';
import { GeocodingService } from './geocoding.service';
import { ApiResponseFormat } from '@app/api-response-formatter/ApiResponseFormat';
import { ApiResponseFormatterService } from '@app/api-response-formatter';

@Controller('geocoding')
export class GeocodingController {
  constructor(
    private geocodingService: GeocodingService,
    private apiResonseFormatter: ApiResponseFormatterService,
  ) {}

  @Get()
  async getGeocodeFromAddress(
    @Query('address') address: string,
  ): Promise<ApiResponseFormat> {
    try {
      const coordinates = await this.geocodingService.getCoordinates(address);
      return this.apiResonseFormatter.formatSuccessResponse([coordinates]);
    } catch (error) {
      return this.apiResonseFormatter.formatErrorResponse(error);
    }
  }
}
