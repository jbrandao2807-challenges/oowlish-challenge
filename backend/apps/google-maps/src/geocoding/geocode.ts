export class Geocode {
  public readonly latitude: string;
  public readonly longitude: string;

  constructor(latitude: string, longitude: string) {
    this.latitude = latitude;
    this.longitude = longitude;
  }
}
