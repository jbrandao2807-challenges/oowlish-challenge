import { HttpService, Injectable, Logger } from '@nestjs/common';

import { Geocode } from './geocode';
import { ConfigService } from '@nestjs/config';
import { ConstantsService } from '@app/constants';

@Injectable()
export class GeocodingService {
  private readonly googleMapsUrl: string;
  private readonly googleMapsApiKey: string;
  private readonly logger: Logger;

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private constants: ConstantsService,
  ) {
    this.logger = new Logger(GeocodingService.name);
    this.googleMapsUrl = this.configService.get<string>(
      this.constants.GOOGLE_MAPS_URL,
    );
    this.googleMapsApiKey = this.configService.get<string>(
      this.constants.GOOGLE_MAPS_API_KEY,
    );
    this.logger.debug(
      `Service started using Google Maps API: ${this.googleMapsUrl}`,
    );
  }

  async getCoordinates(address: string): Promise<any> {
    this.logger.debug(`Address '${address}' - getting geocode`);
    const key = this.googleMapsApiKey;
    try {
      const { data } = await this.httpService
        .get(`${this.googleMapsUrl}/geocode/json`, {
          params: { address, key },
        })
        .toPromise();

      if (data?.status === 'REQUEST_DENIED') {
        throw new Error(data?.error_message);
      }

      if (data?.results.length === 0) {
        this.logger.debug(`Address '${address}' - no results found`);
        return new Geocode('', '');
      }
      this.logger.debug(
        `Address '${address}' - ${data?.results?.length} results found`,
      );
      const firstResult = data?.results[0];
      if (firstResult?.geometry?.location) {
        const { location } = firstResult.geometry;
        return new Geocode(location?.lat, location?.lng);
      }

      throw new Error('Invalid format for geocode on Google Maps API');
    } catch (e) {
      this.logger.error(e.message);
      const geocode = new Geocode('', '');
      return geocode;
    }
  }
}
