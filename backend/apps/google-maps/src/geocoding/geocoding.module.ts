import { HttpModule, Module } from '@nestjs/common';
import { GeocodingService } from './geocoding.service';
import { GeocodingController } from './geocoding.controller';
import { ConstantsModule } from '@app/constants';
import { ApiResponseFormatterModule } from '@app/api-response-formatter';

@Module({
  imports: [HttpModule, ConstantsModule, ApiResponseFormatterModule],
  providers: [GeocodingService],
  controllers: [GeocodingController],
})
export class GeocodingModule {}
