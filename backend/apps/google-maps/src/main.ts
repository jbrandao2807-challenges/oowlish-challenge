import { NestFactory } from '@nestjs/core';
import { GoogleMapsModule } from './google-maps.module';
import { ConfigService } from '@nestjs/config';
import { ConstantsService } from '@app/constants';

async function bootstrap() {
  const isDevelopment = process.env.NODE_ENV === 'development';

  const app = await NestFactory.create(GoogleMapsModule, {
    logger: isDevelopment
      ? ['log', 'error', 'warn', 'debug', 'verbose']
      : ['log', 'error'],
  });
  const configService = app.get(ConfigService);
  const constantsService = app.get(ConstantsService);
  const port = configService.get(
    constantsService.GOOGLE_MAPS_SERVICE_PORT.toString(),
  );
  await app.listen(port);
}

bootstrap();
