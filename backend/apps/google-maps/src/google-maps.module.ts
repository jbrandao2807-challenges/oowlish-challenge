import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GeocodingModule } from './geocoding/geocoding.module';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), GeocodingModule],
})
export class GoogleMapsModule {
}
