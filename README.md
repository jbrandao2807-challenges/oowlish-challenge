# OOWLISH CHALLENGE

- This is the challenge sent by Oowlish on December, 29th.

# Setup

In order to setup the project, you must have Docker and Docker Compose.
Also the .env file needs to be created according to the .env.default file.
#### API KEYS AND CLIENT_IDS ARE NOT PROVIDED!

- COPY .env.default to .env and fill the info needed for API Keys and Client IDS.
- Just run ```docker-compose up``` the first time and the project will setup itself.
