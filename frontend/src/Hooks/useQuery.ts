import * as qs from 'qs';
import { useHistory } from 'react-router-dom';

export const useQuery = () => {
  const history = useHistory();
  const parsedQuery = qs.parse(
    history?.location?.search?.replace('?', '') as string,
    {
      plainObjects: true,
    },
  );
  return parsedQuery;
};
