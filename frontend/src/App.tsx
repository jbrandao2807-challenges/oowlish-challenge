import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import CustomersByCityPage
  from './Pages/customers-by-city/customers-by-city.page';
import { Box } from '@material-ui/core';
import { Route, Router, Switch } from 'react-router-dom';
import { CityCustomersPage } from './Pages/city-customers/city-customers.page';
import { CustomerDetailsPage } from './Pages/customer-details/customer-details.page';
import { createBrowserHistory } from 'history';
import { useAuth0 } from '@auth0/auth0-react';
import { LoginPage } from './Pages/login/login.page';

const history = createBrowserHistory();
const App = () => {
  const handleTitleClick = () => {
    history.push('/');
  };
  const { isAuthenticated } = useAuth0();

  return (
    <Box className='main-app-box'>
      <h1
        className='text-center my-3 main-app-box-title'
        onClick={() => {
          handleTitleClick();
        }}
      >
        Oowlish Challenge Dashboard
      </h1>
      {!isAuthenticated && <LoginPage />}
      {isAuthenticated && (
        <Router history={history}>
          <Switch>
            <Route exact path='/'>
              <CustomersByCityPage />
            </Route>
            <Route exact path='/city'>
              <CityCustomersPage />
            </Route>
            <Route exact path='/customer'>
              <CustomerDetailsPage />
            </Route>
            <Route>
              <h2 className='text-center'>Route not found</h2>
            </Route>
          </Switch>
        </Router>
      )}
    </Box>
  );
};

export default App;
