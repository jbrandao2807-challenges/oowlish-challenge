export const PageHeader = ({ title }: { title: string }) => {
  return <div className='text-center my-3'>
    <h2>{title}</h2>
  </div>;
};