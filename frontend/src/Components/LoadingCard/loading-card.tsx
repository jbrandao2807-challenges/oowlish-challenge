import { Card } from '@material-ui/core';

export const LoadingCard = ({ children, className }: any) => {
  return <Card className={className}>{children}</Card>;
};
