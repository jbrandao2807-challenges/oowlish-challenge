import { LoadingCard } from './loading-card';

export const generateFirstLoadCards = ({
                                         skeletons,
                                         quantity,
                                         className,
                                       }: {
  skeletons: any;
  quantity: number;
  className?: string;
}) => {
  const firstLoadCards = [];
  for (let i = 0; i < quantity; i++) {
    firstLoadCards.push(
      <LoadingCard key={i} className={className}>
        {skeletons}
      </LoadingCard>,
    );
  }
  return firstLoadCards;
};
