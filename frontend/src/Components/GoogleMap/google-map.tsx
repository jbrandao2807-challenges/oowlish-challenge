import * as ReactGoogleMaps from '@react-google-maps/api';
import { GOOGLE_MAPS_API_KEY } from '../../Common/constants';
import { useCallback, useState } from 'react';

declare const window: any;

export const GoogleMap = ({
  latitude,
  longitude,
}: {
  latitude: string;
  longitude: string;
}) => {
  const center = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
  const [map, setMap] = useState(null);
  // const onLoad = () => {};
  // const onUnmount = () => {};
  const containerStyle = {
    width: '100%',
    height: '100%',
  };
  const onLoad = useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds();
    map.fitBounds(bounds);
    setMap(map);
  }, []);

  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);
  return (
    <ReactGoogleMaps.LoadScript
      googleMapsApiKey={GOOGLE_MAPS_API_KEY.toString()}
    >
      <ReactGoogleMaps.GoogleMap
        mapContainerStyle={containerStyle}
        center={center}
        zoom={10}
        onLoad={onLoad}
        onUnmount={onUnmount}
      >
        <ReactGoogleMaps.Marker position={center} />
      </ReactGoogleMaps.GoogleMap>
    </ReactGoogleMaps.LoadScript>
  );
};
