import axios from 'axios';

export const get = async ({
  endpoint,
  params,
}: {
  endpoint: string;
  params?: Record<string, any>;
}) => {
  try {
    const result = await axios.get(endpoint, {
      headers: { 'Access-Control-Allow-Origin': '*' },
      params,
    });
    return result.data;
  } catch (e) {
    console.error(e);
    return null;
  }
};
