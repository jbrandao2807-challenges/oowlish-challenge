export const CUSTOMERS_SERVICE_URL =
  process.env.REACT_APP_CUSTOMERS_SERVICE_URL;

export const UPDATE_PAGE_STATE = 'UPDATE_PAGE_STATE';
export const ITEMS_PER_PAGE = 5;
export const GOOGLE_MAPS_API_KEY =
  process.env.REACT_APP_GOOGLE_MAPS_API_KEY || '';

export const AUTH0_CLIENT_ID = process.env.REACT_APP_AUTH0_CLIENT_ID || '';
export const AUTH0_DOMAIN = process.env.REACT_APP_AUTH0_DOMAIN || '';
