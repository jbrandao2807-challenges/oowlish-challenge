export enum PageState {
  FirstLoad,
  Loading,
  Loaded,
  Error
}