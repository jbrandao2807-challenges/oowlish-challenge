import { PageState } from './PageState';
import { UPDATE_PAGE_STATE } from '../constants';

export const updatePageState = (payload: PageState) => ({
  type: UPDATE_PAGE_STATE,
  payload,
});
