export const CustomerDetailsField = ({
  label,
  value,
}: {
  label: string;
  value: string | undefined;
}) => {
  return (
    <div className='d-flex flex-row justify-content-between'>
      <span>
        <b>{label}</b>
      </span>
      <span>{value}</span>
    </div>
  );
};
