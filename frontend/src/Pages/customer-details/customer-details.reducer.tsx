import { PageState } from '../../Common/PageState/PageState';
import { UPDATE_PAGE_STATE } from '../../Common/constants';
import { ReducerDefaultAction } from '../../Interfaces/ReducerDefaultAction';

export interface ICustomerDetailsInitialState {
  customer: Record<string, any>;
  pageState: PageState;
}

export const CustomerDetailsInitialState: ICustomerDetailsInitialState = {
  customer: {},
  pageState: PageState.FirstLoad,
};
const UPDATE_CUSTOMER_DETAILS = 'UPDATE_CUSTOMER_DETAILS';
export const updateCustomerDetails = (payload: Record<string, any>) => {
  return { type: UPDATE_CUSTOMER_DETAILS, payload };
};

export const CustomerDetailsReducer = (
  state: ICustomerDetailsInitialState,
  action: ReducerDefaultAction,
) => {
  switch (action.type) {
    case UPDATE_PAGE_STATE:
      return { ...state, pageState: action?.payload };
    case UPDATE_CUSTOMER_DETAILS:
      return { ...state, customer: action?.payload };
    default:
      return state;
  }
};
