import { get } from '../../Services/HttpService';
import { CUSTOMERS_SERVICE_URL } from '../../Common/constants';

export const getCustomerDetails = async (customerId: string) => {
  const response = await get({
    endpoint: `${CUSTOMERS_SERVICE_URL}/customers`,
    params: { id: customerId },
  });
  if (response) {
    return response.results;
  } else {
    return [];
  }
};
