import { PageHeader } from '../../Components/PageHeader/PageHeader';
import { CustomerDetailsField } from './customer-details.field';
import { useEffect, useReducer } from 'react';
import {
  CustomerDetailsInitialState,
  CustomerDetailsReducer,
  updateCustomerDetails,
} from './customer-details.reducer';
import { useQuery } from '../../Hooks/useQuery';
import { Card } from '@material-ui/core';
import { getCustomerDetails } from './customer-details.actions';
import { PageState } from '../../Common/PageState/PageState';
import { updatePageState } from '../../Common/PageState/PageState.action';
import { Skeleton } from '@material-ui/lab';
import { GoogleMap } from '../../Components/GoogleMap/google-map';
import './customer-details.css';

export const CustomerDetailsPage = () => {
  const { id: customerId } = useQuery();
  const [state, dispatch] = useReducer(
    CustomerDetailsReducer,
    CustomerDetailsInitialState,
  );
  const {
    first_name,
    last_name,
    email,
    gender,
    company,
    city,
    title,
    latitude,
    longitude,
  }: Record<string, any> = state?.customer;
  useEffect(() => {
    if (customerId) {
      getCustomerDetails(customerId.toString())
        .then((results) => {
          dispatch(updateCustomerDetails(results[0]));
          dispatch(updatePageState(PageState.Loaded));
        })
        .catch((e) => console.error(e));
    }
  }, [customerId]);
  return (
    <>
      <PageHeader title='Customer Details' />
      <Card className='p-3 m-2 text-center'>
        {state.pageState === PageState.FirstLoad && (
          <>
            <span>
              <Skeleton variant='text' />
            </span>
            <span>
              <Skeleton variant='text' />
            </span>{' '}
            <span>
              <Skeleton variant='text' />
            </span>{' '}
            <span>
              <Skeleton variant='text' />
            </span>{' '}
            <span>
              <Skeleton variant='text' />
            </span>{' '}
            <span>
              <Skeleton variant='text' />
            </span>
          </>
        )}
        {state.pageState === PageState.Loaded && (
          <>
            <CustomerDetailsField
              label='Name'
              value={`${first_name || ''} ${last_name || ''}`}
            />
            <CustomerDetailsField label='Title' value={title} />
            <CustomerDetailsField label='Email' value={email} />
            <CustomerDetailsField label='Gender' value={gender} />
            <CustomerDetailsField label='Company' value={company} />
            <CustomerDetailsField label='City' value={city} />
          </>
        )}
      </Card>
      <div className='d-flex flex-row flex-grow-1 customer-details-google-maps '>
        <GoogleMap latitude={latitude} longitude={longitude} />
      </div>
    </>
  );
};
