import { get } from '../../Services/HttpService';
import { CUSTOMERS_SERVICE_URL } from '../../Common/constants';

export const getCustomersByCity = async () => {
  const response = await get({
    endpoint: `${CUSTOMERS_SERVICE_URL}/customers/cities/totals`,
  });
  if (response) {
    const { results } = response;
    const customerTotals = results.map(
      ({
         city,
         customers_total,
       }: {
        city: string;
        customers_total: number;
      }) => ({
        city,
        total: customers_total,
      }),
    );
    customerTotals.sort((itemA: any, itemB: any) => {
      if (itemA.city < itemB.city) {
        return -1;
      }
      if (itemA.city > itemB.city) {
        return 1;
      }
      return 0;
    });
    return customerTotals;
  } else {
    return [];
  }
};
