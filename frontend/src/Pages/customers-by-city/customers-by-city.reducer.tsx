import { PageState } from '../../Common/PageState/PageState';
import { UPDATE_PAGE_STATE } from '../../Common/constants';
import { ReducerDefaultAction } from '../../Interfaces/ReducerDefaultAction';

interface ICustomersByCityInitialState {
  items: any[];
  pageState: PageState;
}

export const CustomersByCityInitialState: ICustomersByCityInitialState = {
  items: [],
  pageState: PageState.FirstLoad,
};
const UPDATE_CUSTOMERS_RESULTS = 'UPDATE_CUSTOMERS_RESULTS';


export const updateCustomersResults = (payload: any) => ({
  type: UPDATE_CUSTOMERS_RESULTS,
  payload,
});
export const CustomersByCityReducer = (
  state: any,
  action: ReducerDefaultAction,
) => {
  switch (action.type) {
    case UPDATE_CUSTOMERS_RESULTS:
      return { ...state, items: action.payload };
    case UPDATE_PAGE_STATE:
      return { ...state, pageState: action.payload };
    default:
      return state;
  }
};
