import { Skeleton } from '@material-ui/lab';

export const CustomersByCityItemCardSkeleton = () => {
  return (
    <>
      <p>
        <Skeleton variant='text' />
      </p>
      <p>
        <Skeleton variant='text' />
      </p>
    </>
  );
};