import { Card } from '@material-ui/core';

interface ICustomersByCityItemCard {
  city: string;
  total: number;
  onClick?: (city: string | undefined) => void;
}

export const CustomersByCityItemCard = ({
  city,
  total,
  onClick,
}: ICustomersByCityItemCard) => {
  return (
    <Card
      className='customer-by-city-card flex-grow-1 p-3 m-2 text-center'
      onClick={(e) => {
        e.preventDefault();
        onClick && onClick(city);
      }}
    >
      <p>{city}</p>
      <p>{total}</p>
    </Card>
  );
};
