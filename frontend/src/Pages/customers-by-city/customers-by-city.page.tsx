import { useEffect, useReducer } from 'react';
import {
  CustomersByCityInitialState,
  CustomersByCityReducer,
  updateCustomersResults,
} from './customers-by-city.reducer';
import { CustomersByCityItemCard } from './customers-by-city.item-card';
import { PageState } from '../../Common/PageState/PageState';
import './customers-by-city.css';
import { getCustomersByCity } from './customers-by-city.actions';
import { useHistory } from 'react-router-dom';
import { generateFirstLoadCards } from '../../Components/LoadingCard/loading-card.generator';
import { PageHeader } from '../../Components/PageHeader/PageHeader';
import { CustomersByCityItemCardSkeleton } from './customers-by-city.item-card-skeleton';
import { updatePageState } from '../../Common/PageState/PageState.action';

export const CustomersByCityPage = (props: any) => {
  const history = useHistory();

  const [state, dispatch] = useReducer(
    CustomersByCityReducer,
    CustomersByCityInitialState,
  );
  const isFirstLoad = state.pageState === PageState.FirstLoad;
  useEffect(() => {
    if (isFirstLoad) {
      getCustomersByCity()
        .then((cities) => {
          dispatch(updateCustomersResults(cities));
          dispatch(updatePageState(PageState.Loaded));
        })
        .catch((err) => {
          dispatch(updatePageState(PageState.Error));
        });
    }
  });
  const handleCardClick = (city: string | undefined) => {
    history.push(`/city?city=${city}`);
  };
  return (
    <>
      <PageHeader title='Cities Report' />
      <div className='d-flex flex-row flex-wrap mx-auto justify-content-center'>
        {isFirstLoad &&
          generateFirstLoadCards({
            skeletons: <CustomersByCityItemCardSkeleton />,
            quantity: 20,
            className: 'customer-by-city-card flex-grow-1 p-3 m-2 text-center',
          })}
        {state.items.length > 0 &&
          state.items.map((item: any, index: number) => (
            <CustomersByCityItemCard
              key={index}
              city={item.city}
              total={item.total}
              onClick={handleCardClick}
            />
          ))}
        {state?.pageState === PageState.Error && <p>Error fetching data</p>}
      </div>
    </>
  );
};

export default CustomersByCityPage;
