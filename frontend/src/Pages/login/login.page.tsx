import { useAuth0 } from '@auth0/auth0-react';
import { Button } from '@material-ui/core';

export const LoginPage = () => {
  const { loginWithRedirect } = useAuth0();
  return (
    <div className='d-flex flex-column my-3 flex-grow-1 justify-content-center'>
      <h2 className='text-center'>You are not logged in, please login</h2>
      <Button color='primary' onClick={() => loginWithRedirect()}>
        Login
      </Button>
    </div>
  );
};
