import { useHistory } from 'react-router-dom';
import {
  CityCustomersInitialState,
  CityCustomersReducer,
  updateCityCustomers,
  updateCurrentPage,
} from './city-customers.reducer';
import { useEffect, useReducer } from 'react';
import { PageState } from '../../Common/PageState/PageState';
import { generateFirstLoadCards } from '../../Components/LoadingCard/loading-card.generator';
import { PageHeader } from '../../Components/PageHeader/PageHeader';
import { CityCustomersCustomerCardSkeleton } from './city-customers.customer-card-skeleton';
import { getCityCustomers } from './city-customers.actions';
import { CityCustomersCustomerCard } from './city-customers.customer-card';
import './city-customers.css';
import { Pagination } from '@material-ui/lab';
import { useQuery } from '../../Hooks/useQuery';
import { updatePageState } from '../../Common/PageState/PageState.action';

export const CityCustomersPage = () => {
  const { city } = useQuery();
  const history = useHistory();
  const [state, dispatch] = useReducer(
    CityCustomersReducer,
    CityCustomersInitialState,
  );

  useEffect(() => {
    const isFirstLoad = state.pageState === PageState.FirstLoad;
    if (isFirstLoad) {
      getCityCustomers(city)
        .then((customers) => {
          dispatch(updatePageState(PageState.Loaded));
          dispatch(updateCityCustomers(customers));
        })
        .catch((error) => {
          console.error(error);
          dispatch(updatePageState(PageState.Error));
        });
    }
  }, [history.location.search, state.pageState]);
  const handlePageChange = (page: number) => {
    dispatch(updateCurrentPage(page));
  };
  const handleCardClick = (id: string | undefined) => {
    history.push(`/customer/?id=${id}`);
  };
  return (
    <>
      <PageHeader title='City Customers' />
      <div className='d-flex flex-row flex-wrap mx-auto justify-content-center'>
        {state.pageState !== PageState.Loaded &&
          generateFirstLoadCards({
            skeletons: <CityCustomersCustomerCardSkeleton />,
            quantity: 10,
            className:
              'city-customers-customer-card flex-grow-1 p-3 m-2 text-center',
          })}
        {state?.pageItems?.length > 0 &&
          state?.pageItems?.map((item: any, index: number) => (
            <CityCustomersCustomerCard
              customer={item}
              key={index}
              onClick={() => handleCardClick(item.id)}
            />
          ))}
      </div>
      <div className='d-flex flex-row flex-wrap mx-auto justify-content-center'>
        {state?.pageState === PageState.Loaded && state?.totalPages > 0 && (
          <Pagination
            count={state.totalPages}
            color={'primary'}
            onChange={(event, page) => {
              handlePageChange(page);
            }}
          />
        )}
      </div>
      {state?.pageState === PageState.Error && <p>Error fetching data</p>}
    </>
  );
};
