import { Skeleton } from '@material-ui/lab';

export const CityCustomersCustomerCardSkeleton = () => {
  return (
    <>
      <span>
        <Skeleton variant='text' />
      </span>
      <span>
        <Skeleton variant='text' />
      </span>
      <span>
        <Skeleton variant='text' />
      </span>
    </>
  );
};
