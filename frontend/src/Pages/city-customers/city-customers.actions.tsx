import { get } from '../../Services/HttpService';
import { CUSTOMERS_SERVICE_URL } from '../../Common/constants';

export const getCityCustomers = async (city: any) => {
  const response = await get({
    endpoint: `${CUSTOMERS_SERVICE_URL}/customers/cities`,
    params: { name: city },
  });
  if (response) {
    return response.results;
  } else {
    return [];
  }
};
