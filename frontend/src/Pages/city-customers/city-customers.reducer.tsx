import { PageState } from '../../Common/PageState/PageState';
import { ITEMS_PER_PAGE, UPDATE_PAGE_STATE } from '../../Common/constants';
import { ReducerDefaultAction } from '../../Interfaces/ReducerDefaultAction';

export interface ICityCustomersState {
  pageState: PageState;
  items: any[];
  pageItems: any[];
  currentPage: number;
  totalPages: number;
}

export const CityCustomersInitialState: ICityCustomersState = {
  pageState: PageState.FirstLoad,
  items: [],
  pageItems: [],
  currentPage: 0,
  totalPages: 0,
};
const UPDATE_CURRENT_PAGE = 'UPDATE_CURRENT_PAGE';
export const updateCurrentPage = (payload: number) => ({
  type: UPDATE_CURRENT_PAGE,
  payload,
});

const UPDATE_CITY_CUSTOMERS = 'UPDATE_CITY_CUSTOMERS';
export const updateCityCustomers = (payload: any) => ({
  type: UPDATE_CITY_CUSTOMERS,
  payload,
});
export const CityCustomersReducer = (
  state: ICityCustomersState,
  action: ReducerDefaultAction,
) => {
  switch (action.type) {
    case UPDATE_CITY_CUSTOMERS:
      return {
        ...state,
        items: action.payload,
        totalPages: Math.ceil(action.payload.length / ITEMS_PER_PAGE),
        currentPage: 0,
        pageItems: action.payload.filter((item: any, index: number) => {
          return index < ITEMS_PER_PAGE;
        }),
      };
    case UPDATE_PAGE_STATE:
      return { ...state, pageState: action.payload };
    case UPDATE_CURRENT_PAGE:
      const startPosition = (action.payload - 1) * ITEMS_PER_PAGE;
      const pageItems = state.items.filter((item, index) => {
        return index >= startPosition && index < startPosition + ITEMS_PER_PAGE;
      });
      return { ...state, currentPage: action.payload - 1, pageItems };
    default:
      return state;
  }
};
