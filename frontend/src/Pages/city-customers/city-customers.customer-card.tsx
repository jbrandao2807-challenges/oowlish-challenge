import { Card } from '@material-ui/core';

export const CityCustomersCustomerCard = ({
  customer,
  onClick,
}: {
  customer: any;
  onClick?: () => void;
}) => {
  return (
    <>
      <Card
        className='city-customers-customer-card flex-grow-1 p-3 m-2 text-center d-flex flex-column'
        onClick={onClick}
      >
        <div className='d-flex flex-row justify-content-between'>
          <span>
            <b>Name:</b>{' '}
          </span>
          <span>
            {customer.first_name} {customer.last_name}
          </span>
        </div>
        <div className='d-flex flex-row justify-content-between'>
          <span>
            <b>Company:</b>{' '}
          </span>
          <span>{customer.company}</span>
        </div>
        <div className='d-flex flex-row justify-content-between'>
          <span>
            <b>Title:</b>{' '}
          </span>
          <span>{customer.title}</span>
        </div>
      </Card>
    </>
  );
};
